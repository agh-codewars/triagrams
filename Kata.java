public class Trigrams {
	 public static String trigrams( String phrase ) {
       /*if(phrase.length()<3)
         return "";
   
        char[] arr = phrase.toCharArray();
        StringBuilder builder = new StringBuilder();
        int fast = 0, slow = 0;
        while (slow<arr.length-2){

            while (fast<=slow+2 && fast<arr.length) {
                if (arr[fast]==' '){
                    builder.append("_");
                }else{
                    builder.append(arr[fast]);
                }
                fast++;
            }
            builder.append(" ");
            slow++;
            fast=slow;
        }
        return builder.toString().trim();
        */
        
        String result = "";
        phrase = phrase.replace(" ","_");
        for (int i = 0; i < phrase.length() - 2; i++){ 
            result += phrase.substring(i,i+3) + " ";
            }
        return result.trim();
    }
    
    
}
